<?php
namespace App\Utils;

class OrdenesArea
{	
	//AREANAME => CONTROLLERNAME
	private static $arrayName = 
	['GENERAL'=>'Ordenes',
	'FLEXO'=> 'OrdenesFlexo',
	'GRABADO'=> 'OrdenesGrabado',
	'IMPRESIONES'=> 'OrdenesImpresiones',
	'LONA'=> 'OrdenesLonas',
	'SERIGRAFIA'=> 'OrdenesSerigrafia',
	'SUBLIMACION'=> 'OrdenesSublimacion',
	'VINIL' => 'OrdenesVinil'];

	public static function getController($areaName)
	{
		return self::$arrayName[$areaName];
	}
	
}