<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $ordenesFlexo->noorden],
                ['confirm' => __('Are you sure you want to delete # {0}?', $ordenesFlexo->noorden)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Ordenes Flexo'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="ordenesFlexo form large-9 medium-8 columns content">
    <?= $this->Form->create($ordenesFlexo) ?>
    <fieldset>
        <legend><?= __('Edit Ordenes Flexo') ?></legend>
        <?php
            echo $this->Form->input('material');
            echo $this->Form->input('laminado');
            echo $this->Form->input('rollosde');
            echo $this->Form->input('cantidad');
            echo $this->Form->input('obs');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
