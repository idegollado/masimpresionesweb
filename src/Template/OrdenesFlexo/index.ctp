<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Ordenes Flexo'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="ordenesFlexo index large-9 medium-8 columns content">
    <h3><?= __('Ordenes Flexo') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('noorden') ?></th>
                <th><?= $this->Paginator->sort('material') ?></th>
                <th><?= $this->Paginator->sort('laminado') ?></th>
                <th><?= $this->Paginator->sort('rollosde') ?></th>
                <th><?= $this->Paginator->sort('cantidad') ?></th>
                <th><?= $this->Paginator->sort('obs') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($ordenesFlexo as $ordenesFlexo): ?>
            <tr>
                <td><?= $this->Number->format($ordenesFlexo->noorden) ?></td>
                <td><?= h($ordenesFlexo->material) ?></td>
                <td><?= h($ordenesFlexo->laminado) ?></td>
                <td><?= $this->Number->format($ordenesFlexo->rollosde) ?></td>
                <td><?= $this->Number->format($ordenesFlexo->cantidad) ?></td>
                <td><?= h($ordenesFlexo->obs) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $ordenesFlexo->noorden]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $ordenesFlexo->noorden]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $ordenesFlexo->noorden], ['confirm' => __('Are you sure you want to delete # {0}?', $ordenesFlexo->noorden)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
