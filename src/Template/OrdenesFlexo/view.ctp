<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Ordenes Flexo'), ['action' => 'edit', $ordenesFlexo->noorden]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Ordenes Flexo'), ['action' => 'delete', $ordenesFlexo->noorden], ['confirm' => __('Are you sure you want to delete # {0}?', $ordenesFlexo->noorden)]) ?> </li>
        <li><?= $this->Html->link(__('List Ordenes Flexo'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Ordenes Flexo'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="ordenesFlexo view large-9 medium-8 columns content">
    <h3><?= h($ordenesFlexo->noorden) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Material') ?></th>
            <td><?= h($ordenesFlexo->material) ?></td>
        </tr>
        <tr>
            <th><?= __('Laminado') ?></th>
            <td><?= h($ordenesFlexo->laminado) ?></td>
        </tr>
        <tr>
            <th><?= __('Obs') ?></th>
            <td><?= h($ordenesFlexo->obs) ?></td>
        </tr>
        <tr>
            <th><?= __('Noorden') ?></th>
            <td><?= $this->Number->format($ordenesFlexo->noorden) ?></td>
        </tr>
        <tr>
            <th><?= __('Rollosde') ?></th>
            <td><?= $this->Number->format($ordenesFlexo->rollosde) ?></td>
        </tr>
        <tr>
            <th><?= __('Cantidad') ?></th>
            <td><?= $this->Number->format($ordenesFlexo->cantidad) ?></td>
        </tr>
    </table>
</div>
