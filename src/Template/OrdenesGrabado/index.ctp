<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Ordenes Grabado'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="ordenesGrabado index large-9 medium-8 columns content">
    <h3><?= __('Ordenes Grabado') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('noorden') ?></th>
                <th><?= $this->Paginator->sort('material') ?></th>
                <th><?= $this->Paginator->sort('cantidad') ?></th>
                <th><?= $this->Paginator->sort('obs') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($ordenesGrabado as $ordenesGrabado): ?>
            <tr>
                <td><?= $this->Number->format($ordenesGrabado->noorden) ?></td>
                <td><?= h($ordenesGrabado->material) ?></td>
                <td><?= $this->Number->format($ordenesGrabado->cantidad) ?></td>
                <td><?= h($ordenesGrabado->obs) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $ordenesGrabado->noorden]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $ordenesGrabado->noorden]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $ordenesGrabado->noorden], ['confirm' => __('Are you sure you want to delete # {0}?', $ordenesGrabado->noorden)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
