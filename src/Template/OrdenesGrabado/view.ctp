<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Ordenes Grabado'), ['action' => 'edit', $ordenesGrabado->noorden]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Ordenes Grabado'), ['action' => 'delete', $ordenesGrabado->noorden], ['confirm' => __('Are you sure you want to delete # {0}?', $ordenesGrabado->noorden)]) ?> </li>
        <li><?= $this->Html->link(__('List Ordenes Grabado'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Ordenes Grabado'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="ordenesGrabado view large-9 medium-8 columns content">
    <h3><?= h($ordenesGrabado->noorden) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Material') ?></th>
            <td><?= h($ordenesGrabado->material) ?></td>
        </tr>
        <tr>
            <th><?= __('Obs') ?></th>
            <td><?= h($ordenesGrabado->obs) ?></td>
        </tr>
        <tr>
            <th><?= __('Noorden') ?></th>
            <td><?= $this->Number->format($ordenesGrabado->noorden) ?></td>
        </tr>
        <tr>
            <th><?= __('Cantidad') ?></th>
            <td><?= $this->Number->format($ordenesGrabado->cantidad) ?></td>
        </tr>
    </table>
</div>
