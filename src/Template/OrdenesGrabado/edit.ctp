<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $ordenesGrabado->noorden],
                ['confirm' => __('Are you sure you want to delete # {0}?', $ordenesGrabado->noorden)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Ordenes Grabado'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="ordenesGrabado form large-9 medium-8 columns content">
    <?= $this->Form->create($ordenesGrabado) ?>
    <fieldset>
        <legend><?= __('Edit Ordenes Grabado') ?></legend>
        <?php
            echo $this->Form->input('material');
            echo $this->Form->input('cantidad');
            echo $this->Form->input('obs');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
