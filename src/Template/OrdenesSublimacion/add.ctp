<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Ordenes Sublimacion'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="ordenesSublimacion form large-9 medium-8 columns content">
    <?= $this->Form->create($ordenesSublimacion) ?>
    <fieldset>
        <legend><?= __('Add Ordenes Sublimacion') ?></legend>
        <?php
            echo $this->Form->input('material');
            echo $this->Form->input('cantidad');
            echo $this->Form->input('obs');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
