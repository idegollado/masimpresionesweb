<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Ordenes Sublimacion'), ['action' => 'edit', $ordenesSublimacion->noorden]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Ordenes Sublimacion'), ['action' => 'delete', $ordenesSublimacion->noorden], ['confirm' => __('Are you sure you want to delete # {0}?', $ordenesSublimacion->noorden)]) ?> </li>
        <li><?= $this->Html->link(__('List Ordenes Sublimacion'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Ordenes Sublimacion'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="ordenesSublimacion view large-9 medium-8 columns content">
    <h3><?= h($ordenesSublimacion->noorden) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Material') ?></th>
            <td><?= h($ordenesSublimacion->material) ?></td>
        </tr>
        <tr>
            <th><?= __('Obs') ?></th>
            <td><?= h($ordenesSublimacion->obs) ?></td>
        </tr>
        <tr>
            <th><?= __('Noorden') ?></th>
            <td><?= $this->Number->format($ordenesSublimacion->noorden) ?></td>
        </tr>
        <tr>
            <th><?= __('Cantidad') ?></th>
            <td><?= $this->Number->format($ordenesSublimacion->cantidad) ?></td>
        </tr>
    </table>
</div>
