<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Ordenes Sublimacion'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="ordenesSublimacion index large-9 medium-8 columns content">
    <h3><?= __('Ordenes Sublimacion') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('noorden') ?></th>
                <th><?= $this->Paginator->sort('material') ?></th>
                <th><?= $this->Paginator->sort('cantidad') ?></th>
                <th><?= $this->Paginator->sort('obs') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($ordenesSublimacion as $ordenesSublimacion): ?>
            <tr>
                <td><?= $this->Number->format($ordenesSublimacion->noorden) ?></td>
                <td><?= h($ordenesSublimacion->material) ?></td>
                <td><?= $this->Number->format($ordenesSublimacion->cantidad) ?></td>
                <td><?= h($ordenesSublimacion->obs) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $ordenesSublimacion->noorden]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $ordenesSublimacion->noorden]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $ordenesSublimacion->noorden], ['confirm' => __('Are you sure you want to delete # {0}?', $ordenesSublimacion->noorden)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
