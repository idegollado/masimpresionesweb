<div class="users form">
<?= $this->Flash->render('auth') ?>
<?= $this->Form->create() ?>
    <fieldset>
        <legend><?= __('Porfavor ingrese su usuario y password') ?></legend>
        <?= $this->Form->input('usuario') ?>
        <?= $this->Form->input('contrasena') ?>
    </fieldset>
<?= $this->Form->button(__('Login')); ?>
<?= $this->Form->end() ?>
</div>