<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Ordenes Lona'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="ordenesLonas index large-9 medium-8 columns content">
    <h3><?= __('Ordenes Lonas') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('noorden') ?></th>
                <th><?= $this->Paginator->sort('bastilla') ?></th>
                <th><?= $this->Paginator->sort('restire') ?></th>
                <th><?= $this->Paginator->sort('bolsa') ?></th>
                <th><?= $this->Paginator->sort('ojillos') ?></th>
                <th><?= $this->Paginator->sort('lonafront') ?></th>
                <th><?= $this->Paginator->sort('lonamesh') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($ordenesLonas as $ordenesLona): ?>
            <tr>
                <td><?= $this->Number->format($ordenesLona->noorden) ?></td>
                <td><?= h($ordenesLona->bastilla) ?></td>
                <td><?= h($ordenesLona->restire) ?></td>
                <td><?= h($ordenesLona->bolsa) ?></td>
                <td><?= $this->Number->format($ordenesLona->ojillos) ?></td>
                <td><?= h($ordenesLona->lonafront) ?></td>
                <td><?= h($ordenesLona->lonamesh) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $ordenesLona->noorden]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $ordenesLona->noorden]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $ordenesLona->noorden], ['confirm' => __('Are you sure you want to delete # {0}?', $ordenesLona->noorden)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
