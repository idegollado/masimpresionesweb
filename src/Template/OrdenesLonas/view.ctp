<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Ordenes Lona'), ['action' => 'edit', $ordenesLona->noorden]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Ordenes Lona'), ['action' => 'delete', $ordenesLona->noorden], ['confirm' => __('Are you sure you want to delete # {0}?', $ordenesLona->noorden)]) ?> </li>
        <li><?= $this->Html->link(__('List Ordenes Lonas'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Ordenes Lona'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="ordenesLonas view large-9 medium-8 columns content">
    <h3><?= h($ordenesLona->noorden) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Otroval') ?></th>
            <td><?= h($ordenesLona->otroval) ?></td>
        </tr>
        <tr>
            <th><?= __('Obs') ?></th>
            <td><?= h($ordenesLona->obs) ?></td>
        </tr>
        <tr>
            <th><?= __('Noorden') ?></th>
            <td><?= $this->Number->format($ordenesLona->noorden) ?></td>
        </tr>
        <tr>
            <th><?= __('Ojillos') ?></th>
            <td><?= $this->Number->format($ordenesLona->ojillos) ?></td>
        </tr>
        <tr>
            <th><?= __('Bastilla') ?></th>
            <td><?= $ordenesLona->bastilla ? __('Yes') : __('No'); ?></td>
         </tr>
        <tr>
            <th><?= __('Restire') ?></th>
            <td><?= $ordenesLona->restire ? __('Yes') : __('No'); ?></td>
         </tr>
        <tr>
            <th><?= __('Bolsa') ?></th>
            <td><?= $ordenesLona->bolsa ? __('Yes') : __('No'); ?></td>
         </tr>
        <tr>
            <th><?= __('Lonafront') ?></th>
            <td><?= $ordenesLona->lonafront ? __('Yes') : __('No'); ?></td>
         </tr>
        <tr>
            <th><?= __('Lonamesh') ?></th>
            <td><?= $ordenesLona->lonamesh ? __('Yes') : __('No'); ?></td>
         </tr>
        <tr>
            <th><?= __('Lonatrans') ?></th>
            <td><?= $ordenesLona->lonatrans ? __('Yes') : __('No'); ?></td>
         </tr>
        <tr>
            <th><?= __('Canvas') ?></th>
            <td><?= $ordenesLona->canvas ? __('Yes') : __('No'); ?></td>
         </tr>
        <tr>
            <th><?= __('Transfer') ?></th>
            <td><?= $ordenesLona->transfer ? __('Yes') : __('No'); ?></td>
         </tr>
        <tr>
            <th><?= __('Vinilbrill') ?></th>
            <td><?= $ordenesLona->vinilbrill ? __('Yes') : __('No'); ?></td>
         </tr>
        <tr>
            <th><?= __('Vinilmatte') ?></th>
            <td><?= $ordenesLona->vinilmatte ? __('Yes') : __('No'); ?></td>
         </tr>
        <tr>
            <th><?= __('Tela') ?></th>
            <td><?= $ordenesLona->tela ? __('Yes') : __('No'); ?></td>
         </tr>
        <tr>
            <th><?= __('Papel') ?></th>
            <td><?= $ordenesLona->papel ? __('Yes') : __('No'); ?></td>
         </tr>
        <tr>
            <th><?= __('Vinilarenado') ?></th>
            <td><?= $ordenesLona->vinilarenado ? __('Yes') : __('No'); ?></td>
         </tr>
        <tr>
            <th><?= __('Viniltrans') ?></th>
            <td><?= $ordenesLona->viniltrans ? __('Yes') : __('No'); ?></td>
         </tr>
        <tr>
            <th><?= __('Otro') ?></th>
            <td><?= $ordenesLona->otro ? __('Yes') : __('No'); ?></td>
         </tr>
    </table>
</div>
