<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Ordenes Lonas'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="ordenesLonas form large-9 medium-8 columns content">
    <?= $this->Form->create($ordenesLona) ?>
    <fieldset>
        <legend><?= __('Add Ordenes Lona') ?></legend>
        <?php
            echo $this->Form->input('bastilla');
            echo $this->Form->input('restire');
            echo $this->Form->input('bolsa');
            echo $this->Form->input('ojillos');
            echo $this->Form->input('lonafront');
            echo $this->Form->input('lonamesh');
            echo $this->Form->input('lonatrans');
            echo $this->Form->input('canvas');
            echo $this->Form->input('transfer');
            echo $this->Form->input('vinilbrill');
            echo $this->Form->input('vinilmatte');
            echo $this->Form->input('tela');
            echo $this->Form->input('papel');
            echo $this->Form->input('vinilarenado');
            echo $this->Form->input('viniltrans');
            echo $this->Form->input('otroval');
            echo $this->Form->input('otro');
            echo $this->Form->input('obs');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
