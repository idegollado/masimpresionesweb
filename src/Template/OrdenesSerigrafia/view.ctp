<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Ordenes Serigrafium'), ['action' => 'edit', $ordenesSerigrafium->noorden]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Ordenes Serigrafium'), ['action' => 'delete', $ordenesSerigrafium->noorden], ['confirm' => __('Are you sure you want to delete # {0}?', $ordenesSerigrafium->noorden)]) ?> </li>
        <li><?= $this->Html->link(__('List Ordenes Serigrafia'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Ordenes Serigrafium'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="ordenesSerigrafia view large-9 medium-8 columns content">
    <h3><?= h($ordenesSerigrafium->noorden) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Material') ?></th>
            <td><?= h($ordenesSerigrafium->material) ?></td>
        </tr>
        <tr>
            <th><?= __('Tintas') ?></th>
            <td><?= h($ordenesSerigrafium->tintas) ?></td>
        </tr>
        <tr>
            <th><?= __('Obs') ?></th>
            <td><?= h($ordenesSerigrafium->obs) ?></td>
        </tr>
        <tr>
            <th><?= __('Noorden') ?></th>
            <td><?= $this->Number->format($ordenesSerigrafium->noorden) ?></td>
        </tr>
        <tr>
            <th><?= __('Cantidad') ?></th>
            <td><?= $this->Number->format($ordenesSerigrafium->cantidad) ?></td>
        </tr>
    </table>
</div>
