<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Ordenes Serigrafium'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="ordenesSerigrafia index large-9 medium-8 columns content">
    <h3><?= __('Ordenes Serigrafia') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('noorden') ?></th>
                <th><?= $this->Paginator->sort('material') ?></th>
                <th><?= $this->Paginator->sort('tintas') ?></th>
                <th><?= $this->Paginator->sort('cantidad') ?></th>
                <th><?= $this->Paginator->sort('obs') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($ordenesSerigrafia as $ordenesSerigrafium): ?>
            <tr>
                <td><?= $this->Number->format($ordenesSerigrafium->noorden) ?></td>
                <td><?= h($ordenesSerigrafium->material) ?></td>
                <td><?= h($ordenesSerigrafium->tintas) ?></td>
                <td><?= $this->Number->format($ordenesSerigrafium->cantidad) ?></td>
                <td><?= h($ordenesSerigrafium->obs) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $ordenesSerigrafium->noorden]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $ordenesSerigrafium->noorden]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $ordenesSerigrafium->noorden], ['confirm' => __('Are you sure you want to delete # {0}?', $ordenesSerigrafium->noorden)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
