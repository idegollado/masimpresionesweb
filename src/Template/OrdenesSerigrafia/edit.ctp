<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $ordenesSerigrafium->noorden],
                ['confirm' => __('Are you sure you want to delete # {0}?', $ordenesSerigrafium->noorden)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Ordenes Serigrafia'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="ordenesSerigrafia form large-9 medium-8 columns content">
    <?= $this->Form->create($ordenesSerigrafium) ?>
    <fieldset>
        <legend><?= __('Edit Ordenes Serigrafium') ?></legend>
        <?php
            echo $this->Form->input('material');
            echo $this->Form->input('tintas');
            echo $this->Form->input('cantidad');
            echo $this->Form->input('obs');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
