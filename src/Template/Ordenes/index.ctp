<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Ordene'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Ordenes Impresiones'), ['controller' => 'OrdenesImpresiones', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Ordenes Impresione'), ['controller' => 'OrdenesImpresiones', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="ordenes index large-9 medium-8 columns content">
    <h3><?= __('Ordenes') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('noorden') ?></th>
                <th><?= $this->Paginator->sort('fecha') ?></th>
                <th><?= $this->Paginator->sort('usaCot') ?></th>
                <th><?= $this->Paginator->sort('noCot') ?></th>
                <th><?= $this->Paginator->sort('clavecliente') ?></th>
                <th><?= $this->Paginator->sort('nombreCliente') ?></th>
                <th><?= $this->Paginator->sort('contacto') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($ordenes as $ordene): ?>
            <tr>
                <td><?= $this->Number->format($ordene->noorden) ?></td>
                <td><?= h($ordene->fecha) ?></td>
                <td><?= h($ordene->usaCot) ?></td>
                <td><?= $this->Number->format($ordene->noCot) ?></td>
                <td><?= $this->Number->format($ordene->clavecliente) ?></td>
                <td><?= h($ordene->nombreCliente) ?></td>
                <td><?= h($ordene->contacto) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $ordene->noorden]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $ordene->noorden]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $ordene->noorden], ['confirm' => __('Are you sure you want to delete # {0}?', $ordene->noorden)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
