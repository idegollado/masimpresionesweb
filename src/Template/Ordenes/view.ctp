<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Ordene'), ['action' => 'edit', $ordene->noorden]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Ordene'), ['action' => 'delete', $ordene->noorden], ['confirm' => __('Are you sure you want to delete # {0}?', $ordene->noorden)]) ?> </li>
        <li><?= $this->Html->link(__('List Ordenes'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Ordene'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Ordenes Impresiones'), ['controller' => 'OrdenesImpresiones', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Ordenes Impresione'), ['controller' => 'OrdenesImpresiones', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="ordenes view large-9 medium-8 columns content">
    <h3><?= h($ordene->noorden) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('NombreCliente') ?></th>
            <td><?= h($ordene->nombreCliente) ?></td>
        </tr>
        <tr>
            <th><?= __('Contacto') ?></th>
            <td><?= h($ordene->contacto) ?></td>
        </tr>
        <tr>
            <th><?= __('Correo') ?></th>
            <td><?= h($ordene->correo) ?></td>
        </tr>
        <tr>
            <th><?= __('Tel') ?></th>
            <td><?= h($ordene->tel) ?></td>
        </tr>
        <tr>
            <th><?= __('Via') ?></th>
            <td><?= h($ordene->via) ?></td>
        </tr>
        <tr>
            <th><?= __('Autorizadopor') ?></th>
            <td><?= h($ordene->autorizadopor) ?></td>
        </tr>
        <tr>
            <th><?= __('Archivo') ?></th>
            <td><?= h($ordene->archivo) ?></td>
        </tr>
        <tr>
            <th><?= __('Obs') ?></th>
            <td><?= h($ordene->obs) ?></td>
        </tr>
        <tr>
            <th><?= __('Prioridad') ?></th>
            <td><?= h($ordene->prioridad) ?></td>
        </tr>
        <tr>
            <th><?= __('Noorden') ?></th>
            <td><?= $this->Number->format($ordene->noorden) ?></td>
        </tr>
        <tr>
            <th><?= __('NoCot') ?></th>
            <td><?= $this->Number->format($ordene->noCot) ?></td>
        </tr>
        <tr>
            <th><?= __('Clavecliente') ?></th>
            <td><?= $this->Number->format($ordene->clavecliente) ?></td>
        </tr>
        <tr>
            <th><?= __('Partidas') ?></th>
            <td><?= $this->Number->format($ordene->partidas) ?></td>
        </tr>
        <tr>
            <th><?= __('Fecha') ?></th>
            <td><?= h($ordene->fecha) ?></td>
        </tr>
        <tr>
            <th><?= __('FechaEntregaDiseno') ?></th>
            <td><?= h($ordene->fechaEntregaDiseno) ?></td>
        </tr>
        <tr>
            <th><?= __('FechaEntrega') ?></th>
            <td><?= h($ordene->fechaEntrega) ?></td>
        </tr>
        <tr>
            <th><?= __('UsaCot') ?></th>
            <td><?= $ordene->usaCot ? __('Yes') : __('No'); ?></td>
         </tr>
    </table>
</div>
