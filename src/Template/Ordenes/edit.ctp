<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $ordene->noorden],
                ['confirm' => __('Are you sure you want to delete # {0}?', $ordene->noorden)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Ordenes'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Ordenes Impresiones'), ['controller' => 'OrdenesImpresiones', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Ordenes Impresione'), ['controller' => 'OrdenesImpresiones', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="ordenes form large-9 medium-8 columns content">
    <?= $this->Form->create($ordene) ?>
    <fieldset>
        <legend><?= __('Edit Ordene') ?></legend>
        <?php
            echo $this->Form->input('fecha');
            echo $this->Form->input('usaCot');
            echo $this->Form->input('noCot');
            echo $this->Form->input('clavecliente');
            echo $this->Form->input('nombreCliente');
            echo $this->Form->input('contacto');
            echo $this->Form->input('correo');
            echo $this->Form->input('tel');
            echo $this->Form->input('fechaEntregaDiseno');
            echo $this->Form->input('via');
            echo $this->Form->input('autorizadopor');
            echo $this->Form->input('fechaEntrega');
            echo $this->Form->input('archivo');
            echo $this->Form->input('obs');
            echo $this->Form->input('partidas');
            echo $this->Form->input('prioridad');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
