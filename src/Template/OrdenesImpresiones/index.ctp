<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Ordenes Impresione'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="ordenesImpresiones index large-9 medium-8 columns content">
    <h3><?= __('Ordenes Impresiones') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('noorden') ?></th>
                <th><?= $this->Paginator->sort('impresion') ?></th>
                <th><?= $this->Paginator->sort('solofrente') ?></th>
                <th><?= $this->Paginator->sort('frenteyvuelta') ?></th>
                <th><?= $this->Paginator->sort('engargolado') ?></th>
                <th><?= $this->Paginator->sort('engomado') ?></th>
                <th><?= $this->Paginator->sort('compaginado') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($ordenesImpresiones as $ordenesImpresione): ?>
            <tr>
                <td><?= $this->Number->format($ordenesImpresione->noorden) ?></td>
                <td><?= h($ordenesImpresione->impresion) ?></td>
                <td><?= h($ordenesImpresione->solofrente) ?></td>
                <td><?= h($ordenesImpresione->frenteyvuelta) ?></td>
                <td><?= h($ordenesImpresione->engargolado) ?></td>
                <td><?= h($ordenesImpresione->engomado) ?></td>
                <td><?= h($ordenesImpresione->compaginado) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $ordenesImpresione->noorden]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $ordenesImpresione->noorden]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $ordenesImpresione->noorden], ['confirm' => __('Are you sure you want to delete # {0}?', $ordenesImpresione->noorden)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
