<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Ordenes Impresiones'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="ordenesImpresiones form large-9 medium-8 columns content">
    <?= $this->Form->create($ordenesImpresione) ?>
    <fieldset>
        <legend><?= __('Add Ordenes Impresione') ?></legend>
        <?php
            echo $this->Form->input('impresion');
            echo $this->Form->input('solofrente');
            echo $this->Form->input('frenteyvuelta');
            echo $this->Form->input('engargolado');
            echo $this->Form->input('engomado');
            echo $this->Form->input('compaginado');
            echo $this->Form->input('corte');
            echo $this->Form->input('cuartocarta');
            echo $this->Form->input('mediacarta');
            echo $this->Form->input('carta');
            echo $this->Form->input('tabloide');
            echo $this->Form->input('oficio');
            echo $this->Form->input('terciooficio');
            echo $this->Form->input('mediooficio');
            echo $this->Form->input('otro1val');
            echo $this->Form->input('otro2val');
            echo $this->Form->input('otro1');
            echo $this->Form->input('otro2');
            echo $this->Form->input('obs');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
