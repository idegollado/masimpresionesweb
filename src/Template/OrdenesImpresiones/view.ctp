<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Ordenes Impresione'), ['action' => 'edit', $ordenesImpresione->noorden]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Ordenes Impresione'), ['action' => 'delete', $ordenesImpresione->noorden], ['confirm' => __('Are you sure you want to delete # {0}?', $ordenesImpresione->noorden)]) ?> </li>
        <li><?= $this->Html->link(__('List Ordenes Impresiones'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Ordenes Impresione'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="ordenesImpresiones view large-9 medium-8 columns content">
    <h3><?= h($ordenesImpresione->noorden) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Impresion') ?></th>
            <td><?= h($ordenesImpresione->impresion) ?></td>
        </tr>
        <tr>
            <th><?= __('Otro1val') ?></th>
            <td><?= h($ordenesImpresione->otro1val) ?></td>
        </tr>
        <tr>
            <th><?= __('Otro2val') ?></th>
            <td><?= h($ordenesImpresione->otro2val) ?></td>
        </tr>
        <tr>
            <th><?= __('Obs') ?></th>
            <td><?= h($ordenesImpresione->obs) ?></td>
        </tr>
        <tr>
            <th><?= __('Noorden') ?></th>
            <td><?= $this->Number->format($ordenesImpresione->noorden) ?></td>
        </tr>
        <tr>
            <th><?= __('Solofrente') ?></th>
            <td><?= $ordenesImpresione->solofrente ? __('Yes') : __('No'); ?></td>
         </tr>
        <tr>
            <th><?= __('Frenteyvuelta') ?></th>
            <td><?= $ordenesImpresione->frenteyvuelta ? __('Yes') : __('No'); ?></td>
         </tr>
        <tr>
            <th><?= __('Engargolado') ?></th>
            <td><?= $ordenesImpresione->engargolado ? __('Yes') : __('No'); ?></td>
         </tr>
        <tr>
            <th><?= __('Engomado') ?></th>
            <td><?= $ordenesImpresione->engomado ? __('Yes') : __('No'); ?></td>
         </tr>
        <tr>
            <th><?= __('Compaginado') ?></th>
            <td><?= $ordenesImpresione->compaginado ? __('Yes') : __('No'); ?></td>
         </tr>
        <tr>
            <th><?= __('Corte') ?></th>
            <td><?= $ordenesImpresione->corte ? __('Yes') : __('No'); ?></td>
         </tr>
        <tr>
            <th><?= __('Cuartocarta') ?></th>
            <td><?= $ordenesImpresione->cuartocarta ? __('Yes') : __('No'); ?></td>
         </tr>
        <tr>
            <th><?= __('Mediacarta') ?></th>
            <td><?= $ordenesImpresione->mediacarta ? __('Yes') : __('No'); ?></td>
         </tr>
        <tr>
            <th><?= __('Carta') ?></th>
            <td><?= $ordenesImpresione->carta ? __('Yes') : __('No'); ?></td>
         </tr>
        <tr>
            <th><?= __('Tabloide') ?></th>
            <td><?= $ordenesImpresione->tabloide ? __('Yes') : __('No'); ?></td>
         </tr>
        <tr>
            <th><?= __('Oficio') ?></th>
            <td><?= $ordenesImpresione->oficio ? __('Yes') : __('No'); ?></td>
         </tr>
        <tr>
            <th><?= __('Terciooficio') ?></th>
            <td><?= $ordenesImpresione->terciooficio ? __('Yes') : __('No'); ?></td>
         </tr>
        <tr>
            <th><?= __('Mediooficio') ?></th>
            <td><?= $ordenesImpresione->mediooficio ? __('Yes') : __('No'); ?></td>
         </tr>
        <tr>
            <th><?= __('Otro1') ?></th>
            <td><?= $ordenesImpresione->otro1 ? __('Yes') : __('No'); ?></td>
         </tr>
        <tr>
            <th><?= __('Otro2') ?></th>
            <td><?= $ordenesImpresione->otro2 ? __('Yes') : __('No'); ?></td>
         </tr>
    </table>
</div>
