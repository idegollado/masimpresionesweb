<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Ordenes Vinil'), ['action' => 'edit', $ordenesVinil->noorden]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Ordenes Vinil'), ['action' => 'delete', $ordenesVinil->noorden], ['confirm' => __('Are you sure you want to delete # {0}?', $ordenesVinil->noorden)]) ?> </li>
        <li><?= $this->Html->link(__('List Ordenes Vinil'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Ordenes Vinil'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="ordenesVinil view large-9 medium-8 columns content">
    <h3><?= h($ordenesVinil->noorden) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Colores') ?></th>
            <td><?= h($ordenesVinil->colores) ?></td>
        </tr>
        <tr>
            <th><?= __('Tamano') ?></th>
            <td><?= h($ordenesVinil->tamano) ?></td>
        </tr>
        <tr>
            <th><?= __('Obs') ?></th>
            <td><?= h($ordenesVinil->obs) ?></td>
        </tr>
        <tr>
            <th><?= __('Noorden') ?></th>
            <td><?= $this->Number->format($ordenesVinil->noorden) ?></td>
        </tr>
        <tr>
            <th><?= __('Cantidad') ?></th>
            <td><?= $this->Number->format($ordenesVinil->cantidad) ?></td>
        </tr>
        <tr>
            <th><?= __('Vinilcorte') ?></th>
            <td><?= $ordenesVinil->vinilcorte ? __('Yes') : __('No'); ?></td>
         </tr>
        <tr>
            <th><?= __('Viniltextil') ?></th>
            <td><?= $ordenesVinil->viniltextil ? __('Yes') : __('No'); ?></td>
         </tr>
        <tr>
            <th><?= __('Vinilreflej') ?></th>
            <td><?= $ordenesVinil->vinilreflej ? __('Yes') : __('No'); ?></td>
         </tr>
        <tr>
            <th><?= __('Acabado') ?></th>
            <td><?= $ordenesVinil->acabado ? __('Yes') : __('No'); ?></td>
         </tr>
    </table>
</div>
