<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Ordenes Vinil'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="ordenesVinil form large-9 medium-8 columns content">
    <?= $this->Form->create($ordenesVinil) ?>
    <fieldset>
        <legend><?= __('Add Ordenes Vinil') ?></legend>
        <?php
            echo $this->Form->input('colores');
            echo $this->Form->input('tamano');
            echo $this->Form->input('cantidad');
            echo $this->Form->input('vinilcorte');
            echo $this->Form->input('viniltextil');
            echo $this->Form->input('vinilreflej');
            echo $this->Form->input('acabado');
            echo $this->Form->input('obs');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
