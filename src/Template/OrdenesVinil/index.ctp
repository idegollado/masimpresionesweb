<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Ordenes Vinil'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="ordenesVinil index large-9 medium-8 columns content">
    <h3><?= __('Ordenes Vinil') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('noorden') ?></th>
                <th><?= $this->Paginator->sort('colores') ?></th>
                <th><?= $this->Paginator->sort('tamano') ?></th>
                <th><?= $this->Paginator->sort('cantidad') ?></th>
                <th><?= $this->Paginator->sort('vinilcorte') ?></th>
                <th><?= $this->Paginator->sort('viniltextil') ?></th>
                <th><?= $this->Paginator->sort('vinilreflej') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($ordenesVinil as $ordenesVinil): ?>
            <tr>
                <td><?= $this->Number->format($ordenesVinil->noorden) ?></td>
                <td><?= h($ordenesVinil->colores) ?></td>
                <td><?= h($ordenesVinil->tamano) ?></td>
                <td><?= $this->Number->format($ordenesVinil->cantidad) ?></td>
                <td><?= h($ordenesVinil->vinilcorte) ?></td>
                <td><?= h($ordenesVinil->viniltextil) ?></td>
                <td><?= h($ordenesVinil->vinilreflej) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $ordenesVinil->noorden]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $ordenesVinil->noorden]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $ordenesVinil->noorden], ['confirm' => __('Are you sure you want to delete # {0}?', $ordenesVinil->noorden)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
