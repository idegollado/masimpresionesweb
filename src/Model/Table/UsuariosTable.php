<?php
namespace App\Model\Table;

use App\Model\Entity\Usuarios;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Usuarios Model
 *
 */
class UsuariosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('usuarios');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->hasOne('Areas');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nombre', 'create')
            ->notEmpty('nombre');

        $validator
            ->allowEmpty('direccion');

        $validator
            ->allowEmpty('telefono');

        $validator
            ->add('fechaNac', 'valid', ['rule' => 'date'])
            ->requirePresence('fechaNac', 'create')
            ->notEmpty('fechaNac');

        $validator
            ->add('fechaAlta', 'valid', ['rule' => 'date'])
            ->requirePresence('fechaAlta', 'create')
            ->notEmpty('fechaAlta');

        $validator
            ->requirePresence('usuario', 'create')
            ->notEmpty('usuario')
            ->add('usuario', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->allowEmpty('contrasena');

        return $validator;
    }
}
