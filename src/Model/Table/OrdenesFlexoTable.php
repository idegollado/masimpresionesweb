<?php
namespace App\Model\Table;

use App\Model\Entity\OrdenesFlexo;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * OrdenesFlexo Model
 *
 */
class OrdenesFlexoTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('ordenes_flexo');
        $this->displayField('noorden');
        $this->primaryKey('noorden');
        $this->belongsTo('Ordenes',[
                'foreignKey' => 'noorden'
            ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('noorden', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('noorden', 'create');

        $validator
            ->requirePresence('material', 'create')
            ->notEmpty('material');

        $validator
            ->requirePresence('laminado', 'create')
            ->notEmpty('laminado');

        $validator
            ->add('rollosde', 'valid', ['rule' => 'numeric'])
            ->requirePresence('rollosde', 'create')
            ->notEmpty('rollosde');

        $validator
            ->add('cantidad', 'valid', ['rule' => 'numeric'])
            ->requirePresence('cantidad', 'create')
            ->notEmpty('cantidad');

        $validator
            ->requirePresence('obs', 'create')
            ->notEmpty('obs');

        return $validator;
    }
}
