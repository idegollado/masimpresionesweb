<?php
namespace App\Model\Table;

use App\Model\Entity\OrdenesLona;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * OrdenesLonas Model
 *
 */
class OrdenesLonasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('ordenes_lonas');
        $this->displayField('noorden');
        $this->primaryKey('noorden');

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('noorden', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('noorden', 'create');

        $validator
            ->add('bastilla', 'valid', ['rule' => 'boolean'])
            ->requirePresence('bastilla', 'create')
            ->notEmpty('bastilla');

        $validator
            ->add('restire', 'valid', ['rule' => 'boolean'])
            ->requirePresence('restire', 'create')
            ->notEmpty('restire');

        $validator
            ->add('bolsa', 'valid', ['rule' => 'boolean'])
            ->requirePresence('bolsa', 'create')
            ->notEmpty('bolsa');

        $validator
            ->add('ojillos', 'valid', ['rule' => 'numeric'])
            ->requirePresence('ojillos', 'create')
            ->notEmpty('ojillos');

        $validator
            ->add('lonafront', 'valid', ['rule' => 'boolean'])
            ->requirePresence('lonafront', 'create')
            ->notEmpty('lonafront');

        $validator
            ->add('lonamesh', 'valid', ['rule' => 'boolean'])
            ->requirePresence('lonamesh', 'create')
            ->notEmpty('lonamesh');

        $validator
            ->add('lonatrans', 'valid', ['rule' => 'boolean'])
            ->requirePresence('lonatrans', 'create')
            ->notEmpty('lonatrans');

        $validator
            ->add('canvas', 'valid', ['rule' => 'boolean'])
            ->requirePresence('canvas', 'create')
            ->notEmpty('canvas');

        $validator
            ->add('transfer', 'valid', ['rule' => 'boolean'])
            ->requirePresence('transfer', 'create')
            ->notEmpty('transfer');

        $validator
            ->add('vinilbrill', 'valid', ['rule' => 'boolean'])
            ->requirePresence('vinilbrill', 'create')
            ->notEmpty('vinilbrill');

        $validator
            ->add('vinilmatte', 'valid', ['rule' => 'boolean'])
            ->requirePresence('vinilmatte', 'create')
            ->notEmpty('vinilmatte');

        $validator
            ->add('tela', 'valid', ['rule' => 'boolean'])
            ->requirePresence('tela', 'create')
            ->notEmpty('tela');

        $validator
            ->add('papel', 'valid', ['rule' => 'boolean'])
            ->requirePresence('papel', 'create')
            ->notEmpty('papel');

        $validator
            ->add('vinilarenado', 'valid', ['rule' => 'boolean'])
            ->requirePresence('vinilarenado', 'create')
            ->notEmpty('vinilarenado');

        $validator
            ->add('viniltrans', 'valid', ['rule' => 'boolean'])
            ->requirePresence('viniltrans', 'create')
            ->notEmpty('viniltrans');

        $validator
            ->requirePresence('otroval', 'create')
            ->notEmpty('otroval');

        $validator
            ->add('otro', 'valid', ['rule' => 'boolean'])
            ->requirePresence('otro', 'create')
            ->notEmpty('otro');

        $validator
            ->requirePresence('obs', 'create')
            ->notEmpty('obs');

        return $validator;
    }
}
