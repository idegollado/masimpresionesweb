<?php
namespace App\Model\Table;

use App\Model\Entity\Ordene;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Ordenes Model
 *
 */
class OrdenesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('ordenes');
        $this->displayField('noorden');
        $this->primaryKey('noorden');
        $this->hasOne('OrdenesImpresiones',[
        	'foreignKey' => 'noorden'
        	]);
        $this->hasOne('OrdenesFlexo',[
        	'foreignKey' => 'noorden'
        	]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('noorden', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('noorden', 'create');

        $validator
            ->add('fecha', 'valid', ['rule' => 'date'])
            ->requirePresence('fecha', 'create')
            ->notEmpty('fecha');

        $validator
            ->add('usaCot', 'valid', ['rule' => 'boolean'])
            ->requirePresence('usaCot', 'create')
            ->notEmpty('usaCot');

        $validator
            ->add('noCot', 'valid', ['rule' => 'numeric'])
            ->requirePresence('noCot', 'create')
            ->notEmpty('noCot');

        $validator
            ->add('clavecliente', 'valid', ['rule' => 'numeric'])
            ->requirePresence('clavecliente', 'create')
            ->notEmpty('clavecliente');

        $validator
            ->requirePresence('nombreCliente', 'create')
            ->notEmpty('nombreCliente');

        $validator
            ->requirePresence('contacto', 'create')
            ->notEmpty('contacto');

        $validator
            ->requirePresence('correo', 'create')
            ->notEmpty('correo');

        $validator
            ->requirePresence('tel', 'create')
            ->notEmpty('tel');

        $validator
            ->add('fechaEntregaDiseno', 'valid', ['rule' => 'date'])
            ->requirePresence('fechaEntregaDiseno', 'create')
            ->notEmpty('fechaEntregaDiseno');

        $validator
            ->requirePresence('via', 'create')
            ->notEmpty('via');

        $validator
            ->requirePresence('autorizadopor', 'create')
            ->notEmpty('autorizadopor');

        $validator
            ->add('fechaEntrega', 'valid', ['rule' => 'date'])
            ->requirePresence('fechaEntrega', 'create')
            ->notEmpty('fechaEntrega');

        $validator
            ->requirePresence('archivo', 'create')
            ->notEmpty('archivo');

        $validator
            ->requirePresence('obs', 'create')
            ->notEmpty('obs');

        $validator
            ->add('partidas', 'valid', ['rule' => 'numeric'])
            ->requirePresence('partidas', 'create')
            ->notEmpty('partidas');

        $validator
            ->requirePresence('prioridad', 'create')
            ->notEmpty('prioridad');

        return $validator;
    }
}
