<?php
namespace App\Model\Table;

use App\Model\Entity\OrdenesImpresione;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * OrdenesImpresiones Model
 *
 */
class OrdenesImpresionesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('ordenes_impresiones');
        $this->displayField('noorden');
        $this->primaryKey('noorden');
        $this->belongsTo('Ordenes',[
                'foreignKey' => 'noorden'
            ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('ordenes_id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('ordenes_id', 'create');

        $validator
            ->requirePresence('impresion', 'create')
            ->notEmpty('impresion');

        $validator
            ->add('solofrente', 'valid', ['rule' => 'boolean'])
            ->requirePresence('solofrente', 'create')
            ->notEmpty('solofrente');

        $validator
            ->add('frenteyvuelta', 'valid', ['rule' => 'boolean'])
            ->requirePresence('frenteyvuelta', 'create')
            ->notEmpty('frenteyvuelta');

        $validator
            ->add('engargolado', 'valid', ['rule' => 'boolean'])
            ->requirePresence('engargolado', 'create')
            ->notEmpty('engargolado');

        $validator
            ->add('engomado', 'valid', ['rule' => 'boolean'])
            ->requirePresence('engomado', 'create')
            ->notEmpty('engomado');

        $validator
            ->add('compaginado', 'valid', ['rule' => 'boolean'])
            ->requirePresence('compaginado', 'create')
            ->notEmpty('compaginado');

        $validator
            ->add('corte', 'valid', ['rule' => 'boolean'])
            ->requirePresence('corte', 'create')
            ->notEmpty('corte');

        $validator
            ->add('cuartocarta', 'valid', ['rule' => 'boolean'])
            ->requirePresence('cuartocarta', 'create')
            ->notEmpty('cuartocarta');

        $validator
            ->add('mediacarta', 'valid', ['rule' => 'boolean'])
            ->requirePresence('mediacarta', 'create')
            ->notEmpty('mediacarta');

        $validator
            ->add('carta', 'valid', ['rule' => 'boolean'])
            ->requirePresence('carta', 'create')
            ->notEmpty('carta');

        $validator
            ->add('tabloide', 'valid', ['rule' => 'boolean'])
            ->requirePresence('tabloide', 'create')
            ->notEmpty('tabloide');

        $validator
            ->add('oficio', 'valid', ['rule' => 'boolean'])
            ->requirePresence('oficio', 'create')
            ->notEmpty('oficio');

        $validator
            ->add('terciooficio', 'valid', ['rule' => 'boolean'])
            ->requirePresence('terciooficio', 'create')
            ->notEmpty('terciooficio');

        $validator
            ->add('mediooficio', 'valid', ['rule' => 'boolean'])
            ->requirePresence('mediooficio', 'create')
            ->notEmpty('mediooficio');

        $validator
            ->allowEmpty('otro1val');

        $validator
            ->allowEmpty('otro2val');

        $validator
            ->add('otro1', 'valid', ['rule' => 'boolean'])
            ->requirePresence('otro1', 'create')
            ->notEmpty('otro1');

        $validator
            ->add('otro2', 'valid', ['rule' => 'boolean'])
            ->requirePresence('otro2', 'create')
            ->notEmpty('otro2');

        $validator
            ->allowEmpty('obs');

        return $validator;
    }
}
