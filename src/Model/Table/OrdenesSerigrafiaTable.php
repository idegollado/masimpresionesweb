<?php
namespace App\Model\Table;

use App\Model\Entity\OrdenesSerigrafium;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * OrdenesSerigrafia Model
 *
 */
class OrdenesSerigrafiaTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('ordenes_serigrafia');
        $this->displayField('noorden');
        $this->primaryKey('noorden');

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('noorden', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('noorden', 'create');

        $validator
            ->requirePresence('material', 'create')
            ->notEmpty('material');

        $validator
            ->requirePresence('tintas', 'create')
            ->notEmpty('tintas');

        $validator
            ->add('cantidad', 'valid', ['rule' => 'numeric'])
            ->requirePresence('cantidad', 'create')
            ->notEmpty('cantidad');

        $validator
            ->requirePresence('obs', 'create')
            ->notEmpty('obs');

        return $validator;
    }
}
