<?php
namespace App\Model\Table;

use App\Model\Entity\OrdenesVinil;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * OrdenesVinil Model
 *
 */
class OrdenesVinilTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('ordenes_vinil');
        $this->displayField('noorden');
        $this->primaryKey('noorden');

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('noorden', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('noorden', 'create');

        $validator
            ->requirePresence('colores', 'create')
            ->notEmpty('colores');

        $validator
            ->requirePresence('tamano', 'create')
            ->notEmpty('tamano');

        $validator
            ->add('cantidad', 'valid', ['rule' => 'numeric'])
            ->requirePresence('cantidad', 'create')
            ->notEmpty('cantidad');

        $validator
            ->add('vinilcorte', 'valid', ['rule' => 'boolean'])
            ->requirePresence('vinilcorte', 'create')
            ->notEmpty('vinilcorte');

        $validator
            ->add('viniltextil', 'valid', ['rule' => 'boolean'])
            ->requirePresence('viniltextil', 'create')
            ->notEmpty('viniltextil');

        $validator
            ->add('vinilreflej', 'valid', ['rule' => 'boolean'])
            ->requirePresence('vinilreflej', 'create')
            ->notEmpty('vinilreflej');

        $validator
            ->add('acabado', 'valid', ['rule' => 'boolean'])
            ->requirePresence('acabado', 'create')
            ->notEmpty('acabado');

        $validator
            ->requirePresence('obs', 'create')
            ->notEmpty('obs');

        return $validator;
    }
}
