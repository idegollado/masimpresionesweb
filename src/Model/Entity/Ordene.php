<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Ordene Entity.
 *
 * @property int $noorden
 * @property \Cake\I18n\Time $fecha
 * @property bool $usaCot
 * @property int $noCot
 * @property int $clavecliente
 * @property string $nombreCliente
 * @property string $contacto
 * @property string $correo
 * @property string $tel
 * @property \Cake\I18n\Time $fechaEntregaDiseno
 * @property string $via
 * @property string $autorizadopor
 * @property \Cake\I18n\Time $fechaEntrega
 * @property string $archivo
 * @property string $obs
 * @property int $partidas
 * @property string $prioridad
 * @property \App\Model\Entity\OrdenesImpresione $ordenes_impresione
 */
class Ordene extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'noorden' => false,
    ];
}
