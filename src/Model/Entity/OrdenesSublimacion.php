<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * OrdenesSublimacion Entity.
 *
 * @property int $noorden
 * @property string $material
 * @property int $cantidad
 * @property string $obs
 */
class OrdenesSublimacion extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'noorden' => false,
    ];
}
