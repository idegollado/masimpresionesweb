<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * OrdenesImpresione Entity.
 *
 * @property int $noorden
 * @property string $impresion
 * @property bool $solofrente
 * @property bool $frenteyvuelta
 * @property bool $engargolado
 * @property bool $engomado
 * @property bool $compaginado
 * @property bool $corte
 * @property bool $cuartocarta
 * @property bool $mediacarta
 * @property bool $carta
 * @property bool $tabloide
 * @property bool $oficio
 * @property bool $terciooficio
 * @property bool $mediooficio
 * @property string $otro1val
 * @property string $otro2val
 * @property bool $otro1
 * @property bool $otro2
 * @property string $obs
 */
class OrdenesImpresione extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'noorden' => false,
    ];
}
