<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * OrdenesLona Entity.
 *
 * @property int $noorden
 * @property bool $bastilla
 * @property bool $restire
 * @property bool $bolsa
 * @property int $ojillos
 * @property bool $lonafront
 * @property bool $lonamesh
 * @property bool $lonatrans
 * @property bool $canvas
 * @property bool $transfer
 * @property bool $vinilbrill
 * @property bool $vinilmatte
 * @property bool $tela
 * @property bool $papel
 * @property bool $vinilarenado
 * @property bool $viniltrans
 * @property string $otroval
 * @property bool $otro
 * @property string $obs
 */
class OrdenesLona extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'noorden' => false,
    ];
}
