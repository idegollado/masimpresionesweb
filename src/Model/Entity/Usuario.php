<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\Auth\DefaultPasswordHasher;

/**
 * Usuario Entity.
 *
 * @property int $id
 * @property string $nombre
 * @property string $direccion
 * @property string $telefono
 * @property \Cake\I18n\Time $fechaNac
 * @property \Cake\I18n\Time $fechaAlta
 * @property string $usuario
 * @property string $contrasena
 */
class Usuario extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

    protected function _setPassword($password)
    {
         return (new DefaultPasswordHasher)->hash($password);
    }

    protected function _setContrasena($value) {
        return (new DefaultPasswordHasher)->hash($value);
    }

}
