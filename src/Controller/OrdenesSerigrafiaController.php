<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * OrdenesSerigrafia Controller
 *
 * @property \App\Model\Table\OrdenesSerigrafiaTable $OrdenesSerigrafia
 */
class OrdenesSerigrafiaController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('ordenesSerigrafia', $this->paginate($this->OrdenesSerigrafia));
        $this->set('_serialize', ['ordenesSerigrafia']);
    }

    /**
     * View method
     *
     * @param string|null $id Ordenes Serigrafium id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $ordenesSerigrafium = $this->OrdenesSerigrafia->get($id, [
            'contain' => []
        ]);
        $this->set('ordenesSerigrafium', $ordenesSerigrafium);
        $this->set('_serialize', ['ordenesSerigrafium']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $ordenesSerigrafium = $this->OrdenesSerigrafia->newEntity();
        if ($this->request->is('post')) {
            $ordenesSerigrafium = $this->OrdenesSerigrafia->patchEntity($ordenesSerigrafium, $this->request->data);
            if ($this->OrdenesSerigrafia->save($ordenesSerigrafium)) {
                $this->Flash->success(__('The ordenes serigrafium has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The ordenes serigrafium could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('ordenesSerigrafium'));
        $this->set('_serialize', ['ordenesSerigrafium']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Ordenes Serigrafium id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $ordenesSerigrafium = $this->OrdenesSerigrafia->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $ordenesSerigrafium = $this->OrdenesSerigrafia->patchEntity($ordenesSerigrafium, $this->request->data);
            if ($this->OrdenesSerigrafia->save($ordenesSerigrafium)) {
                $this->Flash->success(__('The ordenes serigrafium has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The ordenes serigrafium could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('ordenesSerigrafium'));
        $this->set('_serialize', ['ordenesSerigrafium']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Ordenes Serigrafium id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $ordenesSerigrafium = $this->OrdenesSerigrafia->get($id);
        if ($this->OrdenesSerigrafia->delete($ordenesSerigrafium)) {
            $this->Flash->success(__('The ordenes serigrafium has been deleted.'));
        } else {
            $this->Flash->error(__('The ordenes serigrafium could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
