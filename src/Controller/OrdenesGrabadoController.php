<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * OrdenesGrabado Controller
 *
 * @property \App\Model\Table\OrdenesGrabadoTable $OrdenesGrabado
 */
class OrdenesGrabadoController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('ordenesGrabado', $this->paginate($this->OrdenesGrabado));
        $this->set('_serialize', ['ordenesGrabado']);
    }

    /**
     * View method
     *
     * @param string|null $id Ordenes Grabado id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $ordenesGrabado = $this->OrdenesGrabado->get($id, [
            'contain' => []
        ]);
        $this->set('ordenesGrabado', $ordenesGrabado);
        $this->set('_serialize', ['ordenesGrabado']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $ordenesGrabado = $this->OrdenesGrabado->newEntity();
        if ($this->request->is('post')) {
            $ordenesGrabado = $this->OrdenesGrabado->patchEntity($ordenesGrabado, $this->request->data);
            if ($this->OrdenesGrabado->save($ordenesGrabado)) {
                $this->Flash->success(__('The ordenes grabado has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The ordenes grabado could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('ordenesGrabado'));
        $this->set('_serialize', ['ordenesGrabado']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Ordenes Grabado id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $ordenesGrabado = $this->OrdenesGrabado->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $ordenesGrabado = $this->OrdenesGrabado->patchEntity($ordenesGrabado, $this->request->data);
            if ($this->OrdenesGrabado->save($ordenesGrabado)) {
                $this->Flash->success(__('The ordenes grabado has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The ordenes grabado could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('ordenesGrabado'));
        $this->set('_serialize', ['ordenesGrabado']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Ordenes Grabado id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $ordenesGrabado = $this->OrdenesGrabado->get($id);
        if ($this->OrdenesGrabado->delete($ordenesGrabado)) {
            $this->Flash->success(__('The ordenes grabado has been deleted.'));
        } else {
            $this->Flash->error(__('The ordenes grabado could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
