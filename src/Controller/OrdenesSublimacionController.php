<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * OrdenesSublimacion Controller
 *
 * @property \App\Model\Table\OrdenesSublimacionTable $OrdenesSublimacion
 */
class OrdenesSublimacionController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('ordenesSublimacion', $this->paginate($this->OrdenesSublimacion));
        $this->set('_serialize', ['ordenesSublimacion']);
    }

    /**
     * View method
     *
     * @param string|null $id Ordenes Sublimacion id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $ordenesSublimacion = $this->OrdenesSublimacion->get($id, [
            'contain' => []
        ]);
        $this->set('ordenesSublimacion', $ordenesSublimacion);
        $this->set('_serialize', ['ordenesSublimacion']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $ordenesSublimacion = $this->OrdenesSublimacion->newEntity();
        if ($this->request->is('post')) {
            $ordenesSublimacion = $this->OrdenesSublimacion->patchEntity($ordenesSublimacion, $this->request->data);
            if ($this->OrdenesSublimacion->save($ordenesSublimacion)) {
                $this->Flash->success(__('The ordenes sublimacion has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The ordenes sublimacion could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('ordenesSublimacion'));
        $this->set('_serialize', ['ordenesSublimacion']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Ordenes Sublimacion id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $ordenesSublimacion = $this->OrdenesSublimacion->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $ordenesSublimacion = $this->OrdenesSublimacion->patchEntity($ordenesSublimacion, $this->request->data);
            if ($this->OrdenesSublimacion->save($ordenesSublimacion)) {
                $this->Flash->success(__('The ordenes sublimacion has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The ordenes sublimacion could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('ordenesSublimacion'));
        $this->set('_serialize', ['ordenesSublimacion']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Ordenes Sublimacion id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $ordenesSublimacion = $this->OrdenesSublimacion->get($id);
        if ($this->OrdenesSublimacion->delete($ordenesSublimacion)) {
            $this->Flash->success(__('The ordenes sublimacion has been deleted.'));
        } else {
            $this->Flash->error(__('The ordenes sublimacion could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
