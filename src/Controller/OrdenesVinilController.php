<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * OrdenesVinil Controller
 *
 * @property \App\Model\Table\OrdenesVinilTable $OrdenesVinil
 */
class OrdenesVinilController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('ordenesVinil', $this->paginate($this->OrdenesVinil));
        $this->set('_serialize', ['ordenesVinil']);
    }

    /**
     * View method
     *
     * @param string|null $id Ordenes Vinil id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $ordenesVinil = $this->OrdenesVinil->get($id, [
            'contain' => []
        ]);
        $this->set('ordenesVinil', $ordenesVinil);
        $this->set('_serialize', ['ordenesVinil']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $ordenesVinil = $this->OrdenesVinil->newEntity();
        if ($this->request->is('post')) {
            $ordenesVinil = $this->OrdenesVinil->patchEntity($ordenesVinil, $this->request->data);
            if ($this->OrdenesVinil->save($ordenesVinil)) {
                $this->Flash->success(__('The ordenes vinil has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The ordenes vinil could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('ordenesVinil'));
        $this->set('_serialize', ['ordenesVinil']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Ordenes Vinil id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $ordenesVinil = $this->OrdenesVinil->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $ordenesVinil = $this->OrdenesVinil->patchEntity($ordenesVinil, $this->request->data);
            if ($this->OrdenesVinil->save($ordenesVinil)) {
                $this->Flash->success(__('The ordenes vinil has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The ordenes vinil could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('ordenesVinil'));
        $this->set('_serialize', ['ordenesVinil']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Ordenes Vinil id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $ordenesVinil = $this->OrdenesVinil->get($id);
        if ($this->OrdenesVinil->delete($ordenesVinil)) {
            $this->Flash->success(__('The ordenes vinil has been deleted.'));
        } else {
            $this->Flash->error(__('The ordenes vinil could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
