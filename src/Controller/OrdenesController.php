<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Ordenes Controller
 *
 * @property \App\Model\Table\OrdenesTable $Ordenes
 */
class OrdenesController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('ordenes', $this->paginate($this->Ordenes));
        $this->set('_serialize', ['ordenes']);
    }

    /**
     * View method
     *
     * @param string|null $id Ordene id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $ordene = $this->Ordenes->get($id, [
            'contain' => ['OrdenesImpresiones']
        ]);
        $this->set('ordene', $ordene);
        $this->set('_serialize', ['ordene']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $ordene = $this->Ordenes->newEntity();
        if ($this->request->is('post')) {
            $ordene = $this->Ordenes->patchEntity($ordene, $this->request->data);
            if ($this->Ordenes->save($ordene)) {
                $this->Flash->success(__('The ordene has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The ordene could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('ordene'));
        $this->set('_serialize', ['ordene']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Ordene id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $ordene = $this->Ordenes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $ordene = $this->Ordenes->patchEntity($ordene, $this->request->data);
            if ($this->Ordenes->save($ordene)) {
                $this->Flash->success(__('The ordene has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The ordene could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('ordene'));
        $this->set('_serialize', ['ordene']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Ordene id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $ordene = $this->Ordenes->get($id);
        if ($this->Ordenes->delete($ordene)) {
            $this->Flash->success(__('The ordene has been deleted.'));
        } else {
            $this->Flash->error(__('The ordene could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
