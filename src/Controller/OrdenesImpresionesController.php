<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * OrdenesImpresiones Controller
 *
 * @property \App\Model\Table\OrdenesImpresionesTable $OrdenesImpresiones
 */
class OrdenesImpresionesController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Ordenes']
        ];
        $this->set('ordenesImpresiones', $this->paginate($this->OrdenesImpresiones));
        $this->set('_serialize', ['ordenesImpresiones']);
    }

    /**
     * View method
     *
     * @param string|null $id Ordenes Impresione id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $ordenesImpresione = $this->OrdenesImpresiones->get($id, [
            'contain' => []
        ]);
        $this->set('ordenesImpresione', $ordenesImpresione);
        $this->set('_serialize', ['ordenesImpresione']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $ordenesImpresione = $this->OrdenesImpresiones->newEntity();
        if ($this->request->is('post')) {
            $ordenesImpresione = $this->OrdenesImpresiones->patchEntity($ordenesImpresione, $this->request->data);
            if ($this->OrdenesImpresiones->save($ordenesImpresione)) {
                $this->Flash->success(__('The ordenes impresione has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The ordenes impresione could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('ordenesImpresione'));
        $this->set('_serialize', ['ordenesImpresione']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Ordenes Impresione id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $ordenesImpresione = $this->OrdenesImpresiones->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $ordenesImpresione = $this->OrdenesImpresiones->patchEntity($ordenesImpresione, $this->request->data);
            if ($this->OrdenesImpresiones->save($ordenesImpresione)) {
                $this->Flash->success(__('The ordenes impresione has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The ordenes impresione could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('ordenesImpresione'));
        $this->set('_serialize', ['ordenesImpresione']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Ordenes Impresione id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $ordenesImpresione = $this->OrdenesImpresiones->get($id);
        if ($this->OrdenesImpresiones->delete($ordenesImpresione)) {
            $this->Flash->success(__('The ordenes impresione has been deleted.'));
        } else {
            $this->Flash->error(__('The ordenes impresione could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
