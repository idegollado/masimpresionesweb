<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * OrdenesLonas Controller
 *
 * @property \App\Model\Table\OrdenesLonasTable $OrdenesLonas
 */
class OrdenesLonasController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('ordenesLonas', $this->paginate($this->OrdenesLonas));
        $this->set('_serialize', ['ordenesLonas']);
    }

    /**
     * View method
     *
     * @param string|null $id Ordenes Lona id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $ordenesLona = $this->OrdenesLonas->get($id, [
            'contain' => []
        ]);
        $this->set('ordenesLona', $ordenesLona);
        $this->set('_serialize', ['ordenesLona']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $ordenesLona = $this->OrdenesLonas->newEntity();
        if ($this->request->is('post')) {
            $ordenesLona = $this->OrdenesLonas->patchEntity($ordenesLona, $this->request->data);
            if ($this->OrdenesLonas->save($ordenesLona)) {
                $this->Flash->success(__('The ordenes lona has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The ordenes lona could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('ordenesLona'));
        $this->set('_serialize', ['ordenesLona']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Ordenes Lona id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $ordenesLona = $this->OrdenesLonas->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $ordenesLona = $this->OrdenesLonas->patchEntity($ordenesLona, $this->request->data);
            if ($this->OrdenesLonas->save($ordenesLona)) {
                $this->Flash->success(__('The ordenes lona has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The ordenes lona could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('ordenesLona'));
        $this->set('_serialize', ['ordenesLona']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Ordenes Lona id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $ordenesLona = $this->OrdenesLonas->get($id);
        if ($this->OrdenesLonas->delete($ordenesLona)) {
            $this->Flash->success(__('The ordenes lona has been deleted.'));
        } else {
            $this->Flash->error(__('The ordenes lona could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
