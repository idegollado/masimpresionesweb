<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * OrdenesFlexo Controller
 *
 * @property \App\Model\Table\OrdenesFlexoTable $OrdenesFlexo
 */
class OrdenesFlexoController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Ordenes']
        ];
        $this->set('ordenesFlexo', $this->paginate($this->OrdenesFlexo));
        $this->set('_serialize', ['ordenesFlexo']);
    }

    /**
     * View method
     *
     * @param string|null $id Ordenes Flexo id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $ordenesFlexo = $this->OrdenesFlexo->get($id, [
            'contain' => []
        ]);
        $this->set('ordenesFlexo', $ordenesFlexo);
        $this->set('_serialize', ['ordenesFlexo']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $ordenesFlexo = $this->OrdenesFlexo->newEntity();
        if ($this->request->is('post')) {
            $ordenesFlexo = $this->OrdenesFlexo->patchEntity($ordenesFlexo, $this->request->data);
            if ($this->OrdenesFlexo->save($ordenesFlexo)) {
                $this->Flash->success(__('The ordenes flexo has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The ordenes flexo could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('ordenesFlexo'));
        $this->set('_serialize', ['ordenesFlexo']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Ordenes Flexo id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $ordenesFlexo = $this->OrdenesFlexo->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $ordenesFlexo = $this->OrdenesFlexo->patchEntity($ordenesFlexo, $this->request->data);
            if ($this->OrdenesFlexo->save($ordenesFlexo)) {
                $this->Flash->success(__('The ordenes flexo has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The ordenes flexo could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('ordenesFlexo'));
        $this->set('_serialize', ['ordenesFlexo']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Ordenes Flexo id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $ordenesFlexo = $this->OrdenesFlexo->get($id);
        if ($this->OrdenesFlexo->delete($ordenesFlexo)) {
            $this->Flash->success(__('The ordenes flexo has been deleted.'));
        } else {
            $this->Flash->error(__('The ordenes flexo could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
