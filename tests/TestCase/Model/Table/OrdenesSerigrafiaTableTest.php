<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\OrdenesSerigrafiaTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\OrdenesSerigrafiaTable Test Case
 */
class OrdenesSerigrafiaTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ordenes_serigrafia'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('OrdenesSerigrafia') ? [] : ['className' => 'App\Model\Table\OrdenesSerigrafiaTable'];
        $this->OrdenesSerigrafia = TableRegistry::get('OrdenesSerigrafia', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->OrdenesSerigrafia);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
