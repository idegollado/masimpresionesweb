<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * OrdenesSerigrafiaFixture
 *
 */
class OrdenesSerigrafiaFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'ordenes_serigrafia';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'noorden' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'material' => ['type' => 'string', 'length' => 99, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'tintas' => ['type' => 'string', 'length' => 99, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'cantidad' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'obs' => ['type' => 'string', 'length' => 256, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['noorden'], 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'noorden' => 1,
            'material' => 'Lorem ipsum dolor sit amet',
            'tintas' => 'Lorem ipsum dolor sit amet',
            'cantidad' => 1,
            'obs' => 'Lorem ipsum dolor sit amet'
        ],
    ];
}
