<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * OrdenesVinilFixture
 *
 */
class OrdenesVinilFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'ordenes_vinil';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'noorden' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'colores' => ['type' => 'string', 'length' => 99, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'tamano' => ['type' => 'string', 'length' => 99, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'cantidad' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'vinilcorte' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'viniltextil' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'vinilreflej' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'acabado' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'obs' => ['type' => 'string', 'length' => 256, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['noorden'], 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'noorden' => 1,
            'colores' => 'Lorem ipsum dolor sit amet',
            'tamano' => 'Lorem ipsum dolor sit amet',
            'cantidad' => 1,
            'vinilcorte' => 1,
            'viniltextil' => 1,
            'vinilreflej' => 1,
            'acabado' => 1,
            'obs' => 'Lorem ipsum dolor sit amet'
        ],
    ];
}
